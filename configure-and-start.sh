#!/bin/sh

env 
if [ $RAM_MIN -gt 0 ] && [ $RAM_MAX -gt 0 ]; then
	sed -i "s|^JVM_MINIMUM_MEMORY=\"[[:digit:]]*m\"$|JVM_MINIMUM_MEMORY=\"${RAM_MIN}m\"|g" ${BITBUCKET_INSTALL}/bin/setenv.sh
	sed -i "s|^JVM_MAXIMUM_MEMORY=\"[[:digit:]]*m\"$|JVM_MAXIMUM_MEMORY=\"${RAM_MAX}m\"|g" ${BITBUCKET_INSTALL}/bin/setenv.sh
fi

# Fetch backup data over the network
if [ "${BACKUP_HOST}" != "false" ] && [ ! -f ${BITBUCKET_HOME}/DONTSYNC ]; then
	chmod 600 ${BACKUP_KEY_FILE}
	rsync -arcz -vv -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${BACKUP_KEY_FILE}" ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/* ${BITBUCKET_HOME}
	touch ${BITBUCKET_HOME}/DONTSYNC
	rm ${BACKUP_KEY_FILE}
	echo "Synchronized backup data over the network from ${BACKUP_HOST}"
fi

if [ "$BITBUCKET_REMOTE_DEBUG" = "true" ]; then
	echo "set remote debugging port"
	JAVA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=n $JAVA_OPTS"
fi

if [ "$PROXY_NAME" != "false" ]; then
	echo "manipulate proxy name in server.xml"
        xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyName -v ${PROXY_NAME} ${BITBUCKET_INSTALL}/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${BITBUCKET_INSTALL}/conf/server.xml
fi

if [ "$HTTPS" = "true" ]; then
	echo "enable https"
        xmlstarlet ed -i /Server/Service/Connector -t attr -n proxyPort -v 443 ${BITBUCKET_INSTALL}/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${BITBUCKET_INSTALL}/conf/server.xml
        xmlstarlet ed -i /Server/Service/Connector -t attr -n scheme -v https ${BITBUCKET_INSTALL}/conf/server.xml > /tmp/generated-server.xml
        mv /tmp/generated-server.xml ${BITBUCKET_INSTALL}/conf/server.xml
fi

if [ "$IMPORTCERT" = "true" ]; then 
	JAVA_OPTS="-Djavax.net.ssl.trustStore=/opt/jdk/current/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=changeit $JAVA_OPTS"      

	cd ${IMPORTPATH}
	if [ ! -f ${BITBUCKET_INSTALL}/skipcert.conf ]; then
		for i in *
		do 
			/usr/lib/jvm/java-8-oracle/jre/bin/keytool -keystore /usr/lib/jvm/java-8-oracle/jre/lib/security/cacerts -importcert -alias $i -file $i -storepass changeit -noprompt
		done
		touch ${BITBUCKET_INSTALL}/skipcert.conf
	fi
	cd ${BITBUCKET_INSTALL}
fi

#if [ "$NEWRELIC" = "true" ]; then
#	unzip newrelic-java-${NEWRELIC_VERSION}.zip -d ${BITBUCKET_INSTALL}
#
#	cd ${BITBUCKET_INSTALL}/newrelic
#	java -jar newrelic.jar install
#	sed -ri "s/app_name:.*$/app_name: ${NEWRELIC_APP_NAME}/" newrelic.yml
#	sed -ri "s/license_key:[ \t]*'[^']+'/license_key: '${NEWRELIC_LICENSE}'/" newrelic.yml
#fi

#if [ "$WAIT" = "true" ]; then
#
#	is_ready() {
#    		eval "$WAIT_COMMAND"
#	}
#
#	# wait until is ready
#	i=0
#	while ! is_ready; do
#    		i=`expr $i + 1`
#    		if [ $i -ge ${WAIT_LOOPS} ]; then
#        		echo "$(date) - still not ready, giving up"
#        		exit 1
#    		fi
#    	echo "WAITING: $(date) - waiting to be ready for ${WAIT_COMMAND}"
#    	sleep ${WAIT_SLEEP}
#	done
#fi

${BITBUCKET_INSTALL}/bin/start-bitbucket.sh -fg
